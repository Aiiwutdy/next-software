<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-main ">-ข้อมูลภาพสไลด์-</h6>
                        <div class="dropdown no-arrow">
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="chart-area">
                            <div class="table-responsive">
                                <table class="table " id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>ชี่อรูปภาพ</th>
                                            <th>รูปภาพ</th>
                                            <th>ลิ้งค์</th>
                                            <th>ลำดับการแสดง</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>132</td>
                                            <td>132</td>
                                            <td>132</td>
                                            <td>132</td>
                                            <td>132</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title ml-3" id="exampleModalLongTitle">เพิ่มข้อมูลภาพสไลด์</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>ชื่อรูปภาพ</label>
                                        <input type="text" id="slideName" name="slideName" class="form-control" value="" placeholder="ชื่อรูปภาพ" />
                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>รูปภาพ</label>
                                        <input type="text" id="slideImg" name="slideImg" class="form-control" value="" placeholder="รูปภาพ" />
                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>ลิ้งค์</label>
                                        <input type="text" id="slideUrl" name="slideUrl" class="form-control" value="" placeholder="ลิ้งค์" />
                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                    <label>ลำดับการแสดง</label>
                                        <input type="number" id="name" name="name" class="form-control" value="" placeholder="ลำดับการแสดง" />
                                    </div>
                                    <div class="col-md-2"></div>

                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary">ยืนยัน</button>
                    </div>
                </div>
            </div>
        </div>