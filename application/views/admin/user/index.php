<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="container-fluid">



    <div class="row">
        <div class="row">
            <div class="col-xl-12 col-lg-12">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-main ">-ข้อมูลแอดมิน-</h6>
                        <div class="dropdown no-arrow">
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="chart-area">
                            <div class="table-responsive">
                                <table class="table " id="dataTable">
                                    <thead>
                                        <tr>
                                            <th>อีเมล์</th>
                                            <th>ชื่อผู้ใช้</th>
                                            <th>สิทธฺิ์การเข้าถึง</th>
                                            <th>Action</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($get_user as $row) {
                                        ?>
                                            <tr>
                                                <td><?php echo $row['ad_name']; ?></td>
                                                <td><?php echo $row['ad_user']; ?></td>
                                                <td><?php if ($row['ad_status'] == 0) {
                                                        echo 'ใช้งานอยู่';
                                                    } else {
                                                        echo 'ยกเลิกใช้งาน';
                                                    } ?>
                                                </td>
                                                <td>
                                                    <!-- Small button groups (default and split) -->
                                                    <div class="btn-group">
                                                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                                                            Small button
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <a class="dropdown-item" href="#">Action</a>
                                                            <a class="dropdown-item" href="#">Another action</a>
                                                            <a class="dropdown-item" href="#">Something else here</a>
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">




        <!-- Modal -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title ml-3" id="exampleModalLongTitle">เพิ่มข้อมูลผู้ใช้</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>อีเมล์</label>
                                        <input type="email" id="email" name="email" class="form-control" value="อีเมล์" placeholder="อีเมล์" />
                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>รหัสผ่าน</label>
                                        <input type="password" id="password" name="password" class="form-control" value="รหัสผ่าน" placeholder="รหัสผ่าน" />
                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>ชื่อผู้ใช้</label>
                                        <input type="text" id="name" name="name" class="form-control" value="ชื่อผู้ใช้" placeholder="ชื่อผู้ใช้" />
                                    </div>
                                </div>
                                <div class="col-md-2"></div>

                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>สิทธฺิ์การเข้าถึง</label>
                                        <select class="custom-select" id="inputGroupSelect01">
                                            <option selected>สิทธฺิ์การเข้าถึง...</option>
                                            <option value="0">ใช้งานอยู่</option>
                                            <option value="1">ยกเลิกใช้งานอยู่</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2"></div>

                                    <!-- -->
                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary">ยืนยัน</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Page level plugins -->
        <script src="<?= base_url() ?>assets/vendor/chart.js/Chart.min.js"></script>
        <script src="dashboard_chart.js"></script>