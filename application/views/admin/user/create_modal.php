<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg" id="loginModal">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title ml-5" id="myModalLabel">สร้างรายการ</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>

        </div>
        <?php $attrib = ['data-toggle' => 'validator', 'role' => 'form'];

        ?>
        <div class="modal-body">

            <p></p>
            <!-- <div class="row">
                <div class="col-md-6">
                    <div id="logo-con" class="text-center"></div>
                </div>
            </div> -->
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label>อีเมล์</label>
                        <input type="email" id="email" name="email" class="form-control" value="อีเมล์" placeholder="อีเมล์" />
                    </div>
                </div>
                <div class="col-md-2"></div>

                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label>รหัสผ่าน</label>
                        <input type="password" id="password" name="password" class="form-control" value="รหัสผ่าน" placeholder="รหัสผ่าน" />
                    </div>
                </div>
                <div class="col-md-2"></div>

                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label>ชื่อผู้ใช้</label>
                        <input type="text" id="name" name="name" class="form-control" value="ชื่อผู้ใช้" placeholder="ชื่อผู้ใช้" />
                    </div>
                </div>
                <div class="col-md-2"></div>

                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label>ชื่อผู้ใช้</label>
                        <input type="text" id="name" name="name" class="form-control" value="ชื่อผู้ใช้" placeholder="ชื่อผู้ใช้" />
                    </div>
                </div>
                <div class="col-md-2"></div>

                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="form-group">
                        <label>สิทธฺิ์การเข้าถึง</label>
                        <select class="custom-select" id="inputGroupSelect01">
                            <option selected>สิทธฺิ์การเข้าถึง...</option>
                            <option value="0">ใช้งานอยู่</option>
                            <option value="1">ยกเลิกใช้งานอยู่</option>
                        </select>
                    </div>
                    <div class="col-md-2"></div>

                    <!-- -->
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary">ยืนยัน</button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#biller_logo').change(function(event) {
            var biller_logo = $(this).val();
            $('#logo-con').html('<img src="<?= base_url('assets/uploads/logos') ?>/' + biller_logo + '" alt="">');
        });
    });
</script>