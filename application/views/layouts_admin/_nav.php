<!-- Sidebar -->
<ul class="navbar-nav backgroud-nav  sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../admin/index.php">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fab fa-app-store"></i>
        </div>
        <div class="sidebar-brand-text mx-4">-Admin-</div>
    </a>
    <!-- Divider -->
    <hr class="sidebar-divider my-1">
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Admin" aria-expanded="true" aria-controls="Admin">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>จัดการข้อมููลแอดมิน</span>

        </a>
        <div id="Admin" class="collapse" aria-labelledby="Admin" data-parent="#accordionSidebar">
            <div class="backgroud-nav-side py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url(); ?>admin/admin/">รายการทั้งหมด</a>
                <!-- <a class="collapse-item" href="<?= base_url(); ?>Admin/create_admin/" data-toggle="modal" data-target="#createmodal">เพิ่มรายการ</a> -->
                <a class="collapse-item" href="" data-toggle="modal" data-target="#exampleModalCenter">
                    เพิ่มรายการ
                </a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Contrack" aria-expanded="true" aria-controls="Contrack">
            <i class="fas fa-building"></i>
            <span>จัดการข้อมูลบริษัท</span>

        </a>
        <div id="Contrack" class="collapse" aria-labelledby="Contrack" data-parent="#accordionSidebar">
            <div class="backgroud-nav-side py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url(); ?>admin/contrack/">รายการทั้งหมด</a>
                <a class="collapse-item" href="" data-toggle="modal" data-target="#exampleModalCenter">
                    เพิ่มรายการ
                </a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#News" aria-expanded="true" aria-controls="News">
            <i class="fas fa-newspaper"></i>
            <span>จัดการข้อมูลข่าวสาร</span>
        </a>
        <div id="News" class="collapse" aria-labelledby="News" data-parent="#accordionSidebar">
            <div class="backgroud-nav-side py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url(); ?>admin/news/">รายการทั้งหมด</a>
                <a class="collapse-item" href="" data-toggle="modal" data-target="#exampleModalCenter">
                    เพิ่มรายการ
                </a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Service" aria-expanded="true" aria-controls="Service">
            <i class="fab fa-servicestack"></i>
            <span>จัดการข้อมูลบริการ</span>
        </a>
        <div id="Service" class="collapse" aria-labelledby="Service" data-parent="#accordionSidebar">
            <div class="backgroud-nav-side py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url(); ?>admin/service/">รายการทั้งหมด</a>
                <a class="collapse-item" href="" data-toggle="modal" data-target="#exampleModalCenter">เพิ่มรายการ</a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Product" aria-expanded="true" aria-controls="Product">
            <i class="fas fa-cart-arrow-down"></i>
            <span>จัดการข้อมูลสินค้า</span>
        </a>
        <div id="Product" class="collapse" aria-labelledby="Product" data-parent="#accordionSidebar">
            <div class="backgroud-nav-side py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url(); ?>admin/product/">รายการทั้งหมด</a>
                <a class="collapse-item" href="" data-toggle="modal" data-target="#exampleModalCenter">
                    เพิ่มรายการ
                </a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#PhotoSilder" aria-expanded="true" aria-controls="PhotoSilder">
            <i class="fas fa-photo-video"></i>
            <span>จัดการภาพสไลด์</span>
        </a>
        <div id="PhotoSilder" class="collapse" aria-labelledby="PhotoSilder" data-parent="#accordionSidebar">
            <div class="backgroud-nav-side py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url(); ?>admin/slide/">รายการทั้งหมด</a>
                <a class="collapse-item" href="" data-toggle="modal" data-target="#exampleModalCenter">
                    เพิ่มรายการ
                </a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Port" aria-expanded="true" aria-controls="Port">
            <i class="fas fa-map-marker-alt"></i>
            <span>จัดการข้อมูลผลงาน</span>
        </a>
        <div id="Port" class="collapse" aria-labelledby="Port" data-parent="#accordionSidebar">
            <div class="backgroud-nav-side py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url(); ?>admin/port/">รายการทั้งหมด</a>
                <a class="collapse-item" href="" data-toggle="modal" data-target="#exampleModalCenter">
                    เพิ่มรายการ
                </a>
            </div>
        </div>
    </li>
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#Gallery" aria-expanded="true" aria-controls="Gallery">
            <i class="fas fa-image"></i>
            <span>จัดการข้อมูลภาพ</span>
        </a>
        <div id="Gallery" class="collapse" aria-labelledby="Gallery" data-parent="#accordionSidebar">
            <div class="backgroud-nav-side py-2 collapse-inner rounded">
                <a class="collapse-item" href="<?= base_url(); ?>admin/album/">รายการทั้งหมด</a>
                <a class="collapse-item" href="" data-toggle="modal" data-target="#exampleModalCenter">
                    เพิ่มรายการ
                </a>
            </div>
        </div>
    </li>
</ul>
<!-- End of Sidebar -->