<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Port extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('session');
		$this->load->model('admin/admin_model', 'admin');
		if (empty($this->session->userdata('user'))) {
			redirect(base_url() . 'auth');
		}
	}
	public function index()
	{
		// if ($this->session->userdata('user')) {
		$this->load->view('layouts_admin/_header');
		$this->load->view('admin/port/index');
		$this->load->view('layouts_admin/_footer');
		// } else {
		// 	$this->load->view('login_admin');
		// }
	}



}